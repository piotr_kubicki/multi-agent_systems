package ascending_bid;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.WakerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

public class AuctioneerAgent extends Agent {

	private AID auctionHouse;
	private Hashtable bidders;
	private Hashtable items;
	private AuctioneerGui myGui;
	
	protected void setup() {
		items = new Hashtable();
		bidders = new Hashtable();
		
		myGui = new AuctioneerGui(this);
		myGui.showGui();
		
		DFAgentDescription template = new DFAgentDescription();
		ServiceDescription sd = new ServiceDescription();
		sd.setType("auction-house");
		template.addServices(sd);
		
		try {
			DFAgentDescription[] result = DFService.search(this, template);
			if (result.length > 0) {
				auctionHouse = result[0].getName();
			}
		} catch (FIPAException fe) {
			fe.printStackTrace();
		}
		
		System.out.println("Auctioneer " + getAID().getName() + " is here.");
//		Object [] items = getArguments();
		loadItems();

		addBehaviour(new RegisterAuction());
		addBehaviour(new RegisterBider());
		addBehaviour(new UnregisterBider());
	}
	
	protected void takeDown() {
		myGui.dispose();
		System.out.println("Auctioneer " + getAID().getName() + " terminating.");
	}
	
	private void loadItems() {
		String fileName = (String) getArguments()[0];
		File file = new File(fileName);
		
		ArrayList<ArrayList<String>> lines = new ArrayList<>();
		Scanner inputStream;
		
		try {
			inputStream = new Scanner(file);
			
			while(inputStream.hasNext()){
                String line= inputStream.next();
                String[] values = line.split(",");
                // this adds the currently parsed line to the 2-dimensional string array
                ArrayList<String> temp = new ArrayList();
                for (String value : values) {
                	temp.add(value);
                }
                
                lines.add(temp);
            }

            inputStream.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int lineNo = 1;
        for(ArrayList<String> line: lines) {
            int columnNo = 1;
            String name = "";
            
            for (String value : line) {
            	
            	if (columnNo % 2 == 0) {
            		items.put(name, value);
            	} else {
            		name = value;
            	}
                
                columnNo++;
            }
            lineNo++;
        }
        
//        Iterator it = items.entrySet().iterator();
//		
//		while (it.hasNext()) {
//			Map.Entry pair = (Map.Entry) it.next();
//			System.out.println("Key: " + pair.getKey() + " value: " + pair.getValue());
//			
//		}
	}
	
	public void nextItem() {
		addBehaviour(new GetBids());
	}
	
	private class RegisterAuction extends OneShotBehaviour {
		public void action() {
			ACLMessage register = new ACLMessage(ACLMessage.INFORM);
			register.addReceiver(auctionHouse);
			
			try {
				ArrayList auctioneer = new ArrayList();
				auctioneer.add(myAgent.getLocalName());
				auctioneer.add( myAgent.getAID());
				
				register.setContentObject(auctioneer);
				register.setConversationId("auctions-reg");
				myAgent.send(register);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	
	public void startAuction() {
		addBehaviour(new UnregisterAuction());
		addBehaviour(new GetBids());
	}
	
	private class UnregisterAuction extends OneShotBehaviour {

			public void action() {
				ACLMessage unregister = new ACLMessage(ACLMessage.INFORM);
				unregister.addReceiver(auctionHouse);
				unregister.setContent(myAgent.getLocalName());
				unregister.setConversationId("auctions-unreg");
				myAgent.send(unregister);
			}
	}
	
	private class RegisterBider extends CyclicBehaviour {
		
		public void action() {
			MessageTemplate mt = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.PROPOSE),
					MessageTemplate.MatchConversationId("bider-reg"));
			ACLMessage msg = myAgent.receive(mt);
			
			if (msg != null) {
				try {
					ArrayList bider = (ArrayList) msg.getContentObject();
					String name = (String) bider.get(0);
					AID biderAID = (AID) bider.get(1);
					bidders.put(name, biderAID);
					
					ACLMessage reply = msg.createReply();
					reply.setPerformative(ACLMessage.CONFIRM);
					reply.setConversationId("bider-reg");
					reply.setReplyWith(msg.getReplyWith());
					myAgent.send(reply);
					
					myGui.addBidder(name);
				} catch (UnreadableException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	private class UnregisterBider extends CyclicBehaviour {
		
		public void action() {
			MessageTemplate mt = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),
					MessageTemplate.MatchConversationId("bider-unreg"));
			ACLMessage msg = myAgent.receive(mt);
			
			if (msg != null) {
				String name = (String) msg.getContent();
				bidders.remove(name);
			}
		}
	}
	
	private class GetBids extends Behaviour {
		private String bestBidder;
		private int bestBid = 0;
		private int offers = 0;
		private MessageTemplate mt;
		private int step = 1;
		private ArrayList offerents = new ArrayList();
		private String item = null;
		private String price = null;
		private int responses = 0;
		
		public void action() {
			
			Map.Entry pair = null;
			
			if (item == null) {
				pair = (Entry) items.entrySet().iterator().next();
				
				item = (String) pair.getKey();
				price = (String) pair.getValue();
				items.remove(item);
			}
			
			switch (step) {
				case 1:
					ACLMessage proposeItem = new ACLMessage(ACLMessage.INFORM);
					proposeItem.setConversationId("auction");
					
					Object[] itemOffer = { item, price };
					
					try {
						proposeItem.setContentObject(itemOffer);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					myGui.setItem(item);
					myGui.setPrice(price);
					myGui.setWinner("");
					// Add bidders
					Iterator it = bidders.entrySet().iterator();
					
					while (it.hasNext()) {
						pair = (Map.Entry) it.next();
						proposeItem.addReceiver((AID) pair.getValue());
					}
					
					myAgent.send(proposeItem);
					mt = MessageTemplate.MatchConversationId("auction");
					step = 2;
					break;
				case 2: 
					ACLMessage offer = myAgent.receive(mt);
					
					if (offer != null) {
						
						ArrayList bidder;
						String name = null;
						int currentOffer = 0;
						
						try {
							bidder = (ArrayList) offer.getContentObject();
							name = (String) bidder.get(0);
							currentOffer = (int) bidder.get(1);
							
							myGui.changeOffer(name, currentOffer);
						} catch (UnreadableException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						if (offer.getPerformative() == ACLMessage.PROPOSE) {
							offerents.add(bidders.get(name));
							offers++;
							
							if (bestBid == 0 || bestBid < currentOffer) {
								bestBid = currentOffer;
								bestBidder = name;
								System.out.println("Current best: " + bestBid);
							}
						} else if (offer.getPerformative() == ACLMessage.REFUSE) {
							myGui.changeOffer(name, 0);
						}
						
						responses++;
						doWait(2000);
						if (responses >= bidders.size()) {
							if (offers == 1) {
								
								step = 3;
							} else {
								price = ((Integer)bestBid).toString();
								offers = 0;
								step = 1;
							}
						}
						
					} else {
						block();
					}
					
					break;
				case 3:	
					if (bestBidder != null) {
						ACLMessage informWinner = new ACLMessage(ACLMessage.CONFIRM);
						informWinner.addReceiver((AID)bidders.get(bestBidder));
						informWinner.setConversationId("auction");
						informWinner.setContent(item);
						System.out.println((AID)bidders.get(bestBidder));
						myAgent.send(informWinner);
						System.out.println(item + " sold.");
						myGui.setWinner(bestBidder);
						
						ACLMessage informOthers = new ACLMessage(ACLMessage.FAILURE);
//						it = bidders.entrySet().iterator();
//						
//						while (it.hasNext()) {
//							Map.Entry pair = (Map.Entry) it.next();
//							
//							if (!bestBidder.equals(pair.getKey())) {
//								informOthers.addReceiver((AID) pair.getValue());
//							}
//						}
						
						for (int i = 0; i < offerents.size(); i++) {
							informOthers.addReceiver((AID) bidders.get(offerents.get(i)));
						}
						
						informOthers.setConversationId("auction");
						myAgent.send(informOthers);
					} else {
						System.out.println(item + " not sold!");
//						ACLMessage informBidders = new ACLMessage(ACLMessage.FAILURE);
//
//						for (int i = 0; i < offerents.size(); i++) {
//							informBidders.addReceiver((AID) offerents.get(i));
//						}
//						
//						informBidders.setConversationId("auction");
//						myAgent.send(informBidders);
					}
					step = 4;
					
					// start new auction
					if (items.size() > 0) {
						System.out.println("Next Item");
						doWait(5000);
						myAgent.addBehaviour(new GetBids());
					} else {
						doWait(5000);
						ACLMessage acEnd = new ACLMessage(ACLMessage.INFORM);
						
						it = bidders.entrySet().iterator();
						
						while (it.hasNext()) {
							pair = (Map.Entry) it.next();
							acEnd.addReceiver((AID) pair.getValue());
							myGui.removeBidder((String)pair.getKey()); 
							it.remove();
						}
						
						acEnd.setConversationId("auction-end");
						myAgent.send(acEnd);
						
						myAgent.doDelete();
					}
			}
		}

		public boolean done() {
			return (step == 4);
		}
	}
	
//	private void loadItems(Object [] items) {
//		
//		for (int i = 0; i < items.length; i++) {
//			this.items.add(items[i]);
//		}
//	}
	
}


