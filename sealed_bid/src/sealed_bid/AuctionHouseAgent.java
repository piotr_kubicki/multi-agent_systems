package sealed_bid;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;
import java.util.Map.Entry;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

public class AuctionHouseAgent extends Agent {
	private AuctionHouseGui myGui;
	private Hashtable auctioneers;
	private Behaviour auctioneersProviderServer;
	
	protected void setup() {
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		ServiceDescription sd = new ServiceDescription();
		sd.setType("auction-house");
		sd.setName("JADE-auction-house");
		dfd.addServices(sd);
		
		try {
			DFService.register(this, dfd);
		} catch (FIPAException fe) {
			fe.printStackTrace();
		}
		
		auctioneers = new Hashtable();
		myGui = new AuctionHouseGui(this);
		myGui.showGui();
		
		addBehaviour(new AuctioneerRegisterServer());
		addBehaviour(new AuctioneerUnRegisterServer());
//		addBehaviour(new ProvideAuctioneer());
		auctioneersProviderServer = new AuctioneersProviderServer(); 
		
		System.out.println("Auction House " + getAID().getName() + " is here.");
	}
	
	protected void takeDown() {
		try {
			DFService.deregister(this);
		} catch (FIPAException fe) {
			fe.printStackTrace();
		}
		
		System.out.println("Auction House " + getAID()+getName() + " terminating.");
		myGui.dispose();
	}
	
	private class AuctioneerRegisterServer extends CyclicBehaviour {
		
		public void action() {
			MessageTemplate mt = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),
					MessageTemplate.MatchConversationId("auctions-reg"));
			ACLMessage msg = myAgent.receive(mt);
			
			if (msg != null) {
				try {
					ArrayList auctioneer = (ArrayList) msg.getContentObject();
					String name = (String) auctioneer.get(0);
					AID auctioneerAID = (AID) auctioneer.get(1);
					auctioneers.put(name, auctioneerAID);
					myGui.addAuctioneer(name);
				} catch (UnreadableException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if (auctioneers.size() > 0)
					addBehaviour(auctioneersProviderServer);
				
			} else {
				block();
			}
		}
	}
	
	private class AuctioneerUnRegisterServer extends CyclicBehaviour {
		
		public void action() {
			MessageTemplate mt = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),
					MessageTemplate.MatchConversationId("auctions-unreg"));
			ACLMessage msg = myAgent.receive(mt);
			
			if (msg != null) {
				String auctioneer = (String) msg.getContent();
				auctioneers.remove(auctioneer);
				myGui.removeAuctioneer(auctioneer);
					
				System.out.println(auctioneer + " auction starts!");
			} else {
				block();
			}
			
			if (auctioneers.size() < 1) {
				removeBehaviour(auctioneersProviderServer);
			}
		}
	}
	
	private class AuctioneersProviderServer extends CyclicBehaviour {
		
		public void action() {
			MessageTemplate mt = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.CFP),
					MessageTemplate.MatchConversationId("find-auctioneer"));
			
			if (auctioneers.size() > 0) {
				ACLMessage msg = myAgent.receive(mt);
				System.out.println("Got auctioneer request, number of acutions " + auctioneers.size());
				if (msg != null) {
					ACLMessage reply = msg.createReply();
					reply.setReplyWith(msg.getReplyWith());
	
					try {
						reply.setPerformative(ACLMessage.PROPOSE);
						Map.Entry<String, AID> auctioneer = (Entry<String, AID>) auctioneers.entrySet().iterator().next();
						reply.setContentObject(auctioneer.getValue());
						myAgent.send(reply);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				else {
					block();
				}
			}
		}
	}
}
