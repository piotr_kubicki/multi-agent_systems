import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import jade.core.Runtime;

public class Application {
	
	public static void main(String[] args){
		 //Setup the JADE environment
		 Profile myProfile = new ProfileImpl();
		 Runtime myRuntime = Runtime.instance();
		 ContainerController myContainer = myRuntime.createMainContainer(myProfile);
		 try{
		 //Start the agent controller, which is itself an agent (rma)
		 AgentController rma = myContainer.createNewAgent("rma", "jade.tools.rma.rma",
		null);
		 rma.start();
		 AgentController advertiser = myContainer.createNewAgent("advertiser",
				 AdvertiserAgent.class.getCanonicalName(), null);
		 advertiser.start();
		 
		 AgentController seller1 = myContainer.createNewAgent("seller1",
				 BookSellerAgent.class.getCanonicalName(), null);
		 seller1.start();
		 
		 AgentController seller2 = myContainer.createNewAgent("seller2",
				 BookSellerAgent.class.getCanonicalName(), null);
		 seller2.start();
		 
		 AgentController buyer1 = myContainer.createNewAgent("buyer1",
				 BookBuyerAgent.class.getCanonicalName(), null);
		 buyer1.start();
		 
		 AgentController buyer2 = myContainer.createNewAgent("buyer2",
				 BookBuyerAgent.class.getCanonicalName(), null);
		 buyer2.start();
		}catch(Exception e){
		 System.out.println("Exception starting agent: " + e.toString());
		 }
		}
}
