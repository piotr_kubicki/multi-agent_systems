package ascending_bid;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;


class BidderAgentGui extends JFrame {	
	private BidderAgent myAgent;
	
	private JTextField itemField, budgetField;
	private DefaultTableModel items;
	
	BidderAgentGui(BidderAgent a) {
		super(a.getLocalName());
		
		myAgent = a;
		
		JPanel p = new JPanel();
		p.setLayout(new GridLayout(4, 3));
		p.add(new JLabel("Item:"));
		itemField = new JTextField(15);
		p.add(itemField);
		p.add(new JLabel("Budget:"));
		budgetField = new JTextField(15);
		p.add(budgetField);
		getContentPane().add(p, BorderLayout.CENTER);
		
		JButton addButton = new JButton("Add");
		addButton.addActionListener( new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				try {
					String item = itemField.getText().trim();
					int budget = Integer.parseInt(budgetField.getText().trim());
					myAgent.addItem(item, budget);
					itemField.setText("");
					budgetField.setText("");
				}
				catch (Exception e) {
					JOptionPane.showMessageDialog(BidderAgentGui.this, "Invalid values. "+e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); 
				}
			}
		} );
		p = new JPanel();
		p.add(addButton);
		items = new DefaultTableModel();
		items.setColumnCount(2);
		String[] columnHeaderLabel = { "Item", "Budget" };
		items.setColumnIdentifiers(columnHeaderLabel);
		final JTable itemsList = new JTable(items);
		
		JScrollPane itemsCatalogue = new JScrollPane(itemsList);
		p.add(itemsCatalogue);
		getContentPane().add(p, BorderLayout.SOUTH);
		
		// Make the agent terminate when the user closes 
		// the GUI using the button on the upper right corner	
		addWindowListener(new	WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				myAgent.doDelete();
			}
		} );
		
		setResizable(false);
	}
	
	public void showGui() {
		pack();
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int centerX = (int)screenSize.getWidth() / 2;
		int centerY = (int)screenSize.getHeight() / 2;
		setLocation(centerX - getWidth() / 2, centerY - getHeight() / 2);
		super.setVisible(true);
	}	
}
