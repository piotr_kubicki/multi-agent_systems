package ascending_bid;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.JTextField;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JScrollPane;

public class BidderGui extends JFrame {

	private BidderAgent myAgent;
	
	private JPanel contentPane;
	private JTextField itemField;
	private JTextField budgetField;
	private DefaultTableModel items;
	private JScrollPane scrollPane;
	private JTable table;


	/**
	 * Create the frame.
	 */
	public BidderGui(BidderAgent a) {
		super(a.getLocalName());
		
		myAgent = a;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0, 0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JLabel lblItemlabel = new JLabel("Item");
		GridBagConstraints gbc_lblItemlabel = new GridBagConstraints();
		gbc_lblItemlabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblItemlabel.anchor = GridBagConstraints.EAST;
		gbc_lblItemlabel.gridx = 1;
		gbc_lblItemlabel.gridy = 1;
		contentPane.add(lblItemlabel, gbc_lblItemlabel);
		
		itemField = new JTextField();
		itemField.setText("");
		GridBagConstraints gbc_txtItemtextfield = new GridBagConstraints();
		gbc_txtItemtextfield.insets = new Insets(0, 0, 5, 0);
		gbc_txtItemtextfield.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtItemtextfield.gridx = 2;
		gbc_txtItemtextfield.gridy = 1;
		contentPane.add(itemField, gbc_txtItemtextfield);
		itemField.setColumns(10);
		
		JLabel lblBudgetlabel = new JLabel("Budget");
		GridBagConstraints gbc_lblBudgetlabel = new GridBagConstraints();
		gbc_lblBudgetlabel.anchor = GridBagConstraints.EAST;
		gbc_lblBudgetlabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblBudgetlabel.gridx = 1;
		gbc_lblBudgetlabel.gridy = 2;
		contentPane.add(lblBudgetlabel, gbc_lblBudgetlabel);
		
		budgetField = new JTextField();
		budgetField.setText("");
		GridBagConstraints gbc_txtBudgettextfield = new GridBagConstraints();
		gbc_txtBudgettextfield.insets = new Insets(0, 0, 5, 0);
		gbc_txtBudgettextfield.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtBudgettextfield.gridx = 2;
		gbc_txtBudgettextfield.gridy = 2;
		contentPane.add(budgetField, gbc_txtBudgettextfield);
		budgetField.setColumns(10);
		
		JButton btnAddbutton = new JButton("Add");
		GridBagConstraints gbc_btnAddbutton = new GridBagConstraints();
		gbc_btnAddbutton.insets = new Insets(0, 0, 5, 0);
		gbc_btnAddbutton.gridx = 2;
		gbc_btnAddbutton.gridy = 3;
		
		
		scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 2;
		gbc_scrollPane.gridy = 4;
		contentPane.add(scrollPane, gbc_scrollPane);
		
		items = new DefaultTableModel();
		items.setColumnCount(2);
		String[] columnHeaderLabel = { "Item", "Budget" };
		items.setColumnIdentifiers(columnHeaderLabel);
		
		table = new JTable(items);
		scrollPane.setViewportView(table);
		
		btnAddbutton = new JButton("Add");
		btnAddbutton.addActionListener( new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				try {
					String item = itemField.getText().trim();
					int budget = Integer.parseInt(budgetField.getText().trim());
					myAgent.addItem(item, budget);
					itemField.setText("");
					budgetField.setText("");
					addItem(item, ((Integer)budget).toString());
				}
				catch (Exception e) {
					JOptionPane.showMessageDialog(BidderGui.this, "Invalid values. "+e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); 
				}
			}
		} );
		contentPane.add(btnAddbutton, gbc_btnAddbutton);
	}
	
	public void addItem(String item, String budget) {
		String [] newItem = {item, budget};
		items.addRow(newItem);
	}
	
	public void removeItem(String item) {
		ArrayList i = getRowsByValues(items, item);
		items.removeRow((int)i.get(0));
	}
	
	private ArrayList getRowsByValues(TableModel model, Object name) {
		ArrayList indexes = new ArrayList();
		
	    for (int i = model.getRowCount() - 1; i >= 0; --i) {
	        if (model.getValueAt(i, 0).equals(name)) {
	               
	        	indexes.add(i);
	        }
	    }
	    
	    return indexes;
	 }
	
	public void showGui() {
		pack();
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int centerX = (int)screenSize.getWidth() / 2;
		int centerY = (int)screenSize.getHeight() / 2;
		setLocation(centerX - getWidth() / 2, centerY - getHeight() / 2);
		super.setVisible(true);
	}	

}
