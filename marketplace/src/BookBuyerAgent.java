import jade.core.Agent;
import jade.core.AID;
import jade.core.behaviours.*;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

import java.util.*;

public class BookBuyerAgent extends Agent {
	
	private String targetBookTitle;
//	private AID seller;
	private AID advertiser;
	private int budget;
	private BookBuyerGui myGui;
	
	protected void setup() {
		System.out.println("Hello! Buyer-agent " + getAID().getName() + " is ready.");

//		Object[] args = getArguments();
		
//		if (args != null && args.length > 0) {
//			targetBookTitle = (String) args[0];
//			System.out.println("Trying to buy " + targetBookTitle);
//			
//			addBehaviour(new TickerBehaviour(this, 8000) {
//				protected void onTick() {
//					myAgent.addBehaviour(new RequestPerformer());
//				}
//			});
//		} else {
//			System.out.println("No book title specified");
//			doDelete();
//		}
		
		DFAgentDescription template = new DFAgentDescription();
		ServiceDescription sd = new ServiceDescription();
		sd.setType("book-selling");
		template.addServices(sd);
		
		try {
			DFAgentDescription[] result = DFService.search(this, template);
			advertiser = result[0].getName();
		} catch (FIPAException fe) {
			fe.printStackTrace();
		}
		
		myGui = new BookBuyerGui(this);
		myGui.showGui();
	}
	
	protected void takeDown() {
		System.out.println("Buyer-agent " + getAID().getName() + " terminating.");
		myGui.dispose();
	}
	
	public void bookRequest(String title, int budget) {
		if (title.length() > 0) {
			if (budget > 0) {
				targetBookTitle = title;
				System.out.println("Trying to buy " + targetBookTitle);
				this.budget = budget;
				
				addBehaviour(new TickerBehaviour(this, 8000) {
					protected void onTick() {
						myAgent.addBehaviour(new RequestPerformer());
					}
				});
			} else {
				System.out.println("Books are not for free!");
				doDelete();
			}
		} else {
			System.out.println("No book title specified");
			doDelete();
		}
	}
	
	private class RequestPerformer extends Behaviour {
		private AID seller;
		private MessageTemplate mt;
		private int step = 0;
		
		public void action() {
			switch (step) {
			case 0:
				ACLMessage cfp = new ACLMessage(ACLMessage.CFP);
				
				cfp.addReceiver(advertiser);
				
				cfp.setContent(targetBookTitle);
				cfp.setConversationId("book-trade");
				cfp.setReplyWith("cfp" + System.currentTimeMillis());
				myAgent.send(cfp);
				
				mt = MessageTemplate.and(MessageTemplate.MatchConversationId("book-trade"),
						MessageTemplate.MatchInReplyTo(cfp.getReplyWith()));
				
				step = 1;
				break;
			case 1:
				ACLMessage reply = myAgent.receive(mt);
				
				if (reply != null) {
					if (reply.getPerformative() == ACLMessage.INFORM) {
						try {
							ArrayList<ArrayList> offers = (ArrayList) reply.getContentObject();
							int lowerPrice = 0;
							AID bestSeller = null;
//							System.out.println("Got " + offers.size() + " offers");
							for (ArrayList offer : offers) {
								int price = (Integer) offer.get(1);
								if (lowerPrice == 0 || lowerPrice > price) {
									lowerPrice = price;
									bestSeller = (AID) offer.get(2);
								} 
							}
//							System.out.println("Decision made");
							if (lowerPrice <= budget && bestSeller != null) {
//								System.out.println("Got my best seller within budget price " + lowerPrice);
								seller = bestSeller;
								step = 2;
								doWait(4000);
							}
						} catch (UnreadableException e) {
							e.printStackTrace();
						}

					}
				} else {
					block();
				}
				break;
			case 2:
				ACLMessage order = new ACLMessage(ACLMessage.ACCEPT_PROPOSAL);
				order.addReceiver(seller);
				order.setContent(targetBookTitle);
				order.setConversationId("book-trade");
				order.setReplyWith("order" + System.currentTimeMillis());
				myAgent.send(order);
				
				mt = MessageTemplate.and(MessageTemplate.MatchConversationId("book-trade"),
						MessageTemplate.MatchInReplyTo(order.getReplyWith()));
				
				step= 3;
				break;
			case 3:
				reply = myAgent.receive(mt);
				
				if (reply != null) {
					if (reply.getPerformative() == ACLMessage.INFORM) {
						System.out.println(targetBookTitle + " successfully purchased.");
						myAgent.doDelete();
					} else if (reply.getPerformative() == ACLMessage.FAILURE) {
						System.out.println("Hmmm... book sold, lets hava a look for different offers.");
						step = 0;
					}
					step = 4;
				} else {
					block();
				}
				break;
			} 
		}
		
		
		public boolean done() {
			return ((step == 2 && seller == null) || step == 4);
		}
	}

}
