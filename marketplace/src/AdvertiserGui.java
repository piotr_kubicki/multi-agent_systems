import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

public class AdvertiserGui extends JFrame {
	private AdvertiserAgent myAgent;
	private DefaultTableModel books;
	
	AdvertiserGui(AdvertiserAgent a) {
		super(a.getLocalName());
		
		myAgent = a;
		
		JPanel p = new JPanel();
		p.setLayout(new GridLayout(1, 1));
		
		getContentPane().add(p, BorderLayout.CENTER);
		
		// Make the agent terminate when the user closes 
		// the GUI using the button on the upper right corner	
		addWindowListener(new	WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				myAgent.doDelete();
			}
		} );
		
		books = new DefaultTableModel();
		books.setColumnCount(2);
		String[] columnHeaderLabel = {"Title", "Price"};
		books.setColumnIdentifiers(columnHeaderLabel);
		
		final JTable booksList = new JTable(books);
		
		JScrollPane booksCatalogue = new JScrollPane(booksList);
		
		add(booksCatalogue);
		
		setResizable(false);
	}
	
	public void addBook(String title, String price) {
		String [] book = {title, price};
		books.addRow(book);
	}
	
	public void removeBook(String title, String price) {
		ArrayList items = getRowsByValues(books, title, price);
		books.removeRow((int)items.get(0));
	}
	
	private ArrayList getRowsByValues(TableModel model, Object title, Object price) {
		ArrayList indexes = new ArrayList();
		
	    for (int i = model.getRowCount() - 1; i >= 0; --i) {
	        if (model.getValueAt(i, 0).equals(title) && model.getValueAt(i, 1).equals(price)) {
	               
	        	indexes.add(i);
	        }
	    }
	    
	    return indexes;
	 }
	
	public void showGui() {
		pack();
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int centerX = (int)screenSize.getWidth() / 2;
		int centerY = (int)screenSize.getHeight() / 2;
		setLocation(centerX - getWidth() / 2, centerY - getHeight() / 2);
		super.setVisible(true);
	}	
}
