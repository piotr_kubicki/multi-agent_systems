package ascending_bid;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

public class AuctioneerGui extends JFrame {
	private AuctioneerAgent myAgent;
	private DefaultTableModel bidders;
	private JLabel itemLabel, priceLabel, winnerLabel;
	
	public AuctioneerGui(AuctioneerAgent a) {
		super(a.getLocalName());
		
		myAgent = a;
		
		JPanel p = new JPanel();
		p.setLayout(new GridLayout(1, 3));
		
		getContentPane().add(p, BorderLayout.CENTER);
		
		// Make the agent terminate when the user closes 
		// the GUI using the button on the upper right corner	
		addWindowListener(new	WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				myAgent.doDelete();
			}
		} );
		
		bidders = new DefaultTableModel();
		bidders.setColumnCount(2);
		String[] columnHeaderLabel = { "Bidders", "Offers" };
		bidders.setColumnIdentifiers(columnHeaderLabel);
		
		final JTable biddersList = new JTable(bidders);
		
		JScrollPane biddersCatalogue = new JScrollPane(biddersList);
		
		add(biddersCatalogue);
		
		JButton startButton = new JButton("Start");
		startButton.addActionListener( new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				try {
					myAgent.startAuction();
				}
				catch (Exception e) {
					JOptionPane.showMessageDialog(AuctioneerGui.this, "Invalid values. "+e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); 
				}
			}
		} );
		
		p = new JPanel();
		p.add(new JLabel("Current item:"));
		itemLabel = new JLabel();
		p.add(itemLabel);
		
		p.add(new JLabel("Current price: "));
		priceLabel = new JLabel();
		p.add(priceLabel);
		
		p.add(new JLabel("Winner:"));
		winnerLabel = new JLabel();
		p.add(winnerLabel);

		p.add(startButton);
		
		getContentPane().add(p, BorderLayout.SOUTH);

		
		setResizable(false);
	}
	
	public void setItem(String item) {
		itemLabel.setText(item);
	}
	
	public void setWinner(String winner) {
		winnerLabel.setText(winner);
	}
	
	public void setPrice(String price) {
		priceLabel.setText(price);
	}
	
	public void addBidder(String name) {
		String [] bidder = {name, "0"};
		bidders.addRow(bidder);
	}
	
	public void removeBidder(String bidder) {
		ArrayList items = getRowsByValues(bidders, bidder);
		bidders.removeRow((int)items.get(0));
	}
	
	public void changeOffer(Object name, int offer) {
		ArrayList indexes = getRowsByValues(bidders, name);
		bidders.setValueAt(offer, (int) indexes.get(0), 1);
	}
	
	private ArrayList getRowsByValues(TableModel model, Object name) {
		ArrayList indexes = new ArrayList();
		
	    for (int i = model.getRowCount() - 1; i >= 0; --i) {
	        if (model.getValueAt(i, 0).equals(name)) {
	               
	        	indexes.add(i);
	        }
	    }
	    
	    return indexes;
	 }
	
	public void showGui() {
		pack();
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int centerX = (int)screenSize.getWidth() / 2;
		int centerY = (int)screenSize.getHeight() / 2;
		setLocation(centerX - getWidth() / 2, centerY - getHeight() / 2);
		super.setVisible(true);
	}	
}

