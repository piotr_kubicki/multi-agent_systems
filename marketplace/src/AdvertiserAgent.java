
import java.io.IOException;
import java.util.*;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

public class AdvertiserAgent extends Agent {
	private ArrayList<ArrayList> catalogue;
	private AdvertiserGui myGui;
	
	protected void setup() {
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		ServiceDescription sd = new ServiceDescription();
		sd.setType("book-selling");
		sd.setName("JADE-book-trading");
		dfd.addServices(sd);
		
		try {
			DFService.register(this, dfd);
		} catch (FIPAException fe) {
			fe.printStackTrace();
		}
		
		catalogue = new ArrayList();
		myGui = new AdvertiserGui(this);
		myGui.showGui();
		
		addBehaviour(new BookRegisterServer());
		addBehaviour(new OfferRequestsServer());
		addBehaviour(new BookUnregisterServer());
		
		System.out.println("Advertiser " + getAID().getName() + " is here.");
	}
	
	protected void takeDown() {
		try {
			DFService.deregister(this);
		} catch (FIPAException fe) {
			fe.printStackTrace();
		}
		
		System.out.println("Advertiser " + getAID()+getName() + " terminating.");
		myGui.dispose();
	}
	
	private class BookRegisterServer extends CyclicBehaviour {
		
		public void action() {
			MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
			ACLMessage msg = myAgent.receive(mt);
			
			if (msg != null) {
				ArrayList book;
				try {
					book = (ArrayList) msg.getContentObject();
					String title = (String) book.get(0);
					String price = book.get(1).toString();
					catalogue.add(book);
					myGui.addBook(title, price);
					
				} catch (UnreadableException e) {
					e.printStackTrace();
				}
				
			} else {
				block();
			}
		}
	}
	
	private class OfferRequestsServer extends CyclicBehaviour {
		
		public void action() {
			MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.CFP);
			ACLMessage msg = myAgent.receive(mt);
			
			if (msg != null) {
				String title = msg.getContent();
				ACLMessage reply = msg.createReply();
				
//				ArrayList book = (ArrayList) catalogue.get(title);
				// Get all matching books
				ArrayList books = new ArrayList();
				for (ArrayList book : catalogue) {
					if (book.get(0).equals(title)) {
						books.add(book);
					}
				}

				if (books.size() > 0) {
					reply.setPerformative(ACLMessage.INFORM);
					try {
						reply.setContentObject(books);
					} catch (IOException e) {
						e.printStackTrace();
					}
				} else {
					reply.setPerformative(ACLMessage.REFUSE);
					reply.setContent("non-available");
				}
				
				myAgent.send(reply);
			} else {
				block();
			}
		}
	}
	
	private class BookUnregisterServer extends CyclicBehaviour {
		
		public void action() {
			MessageTemplate mt = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),
					MessageTemplate.MatchConversationId("book-unreg"));
			ACLMessage msg = myAgent.receive(mt);
			
			if (msg != null) {
				ArrayList book;
				try {
					book = (ArrayList) msg.getContentObject();
					String title = (String) book.get(0);
					String price = book.get(1).toString();
//					int toRemove = -1;
					
					for (ArrayList item : catalogue) {
//						toRemove++;
						
						if (item.get(0).equals(title) && item.get(1).equals(Integer.parseInt(price))) {
							catalogue.remove(item);
							break;
						}
						
					}
					
//					if (toRemove >= 0) {
//						ArrayList r = catalogue.remove(toRemove);
//						System.out.println("Removed price: " + r.get(1));
//						
//						for(ArrayList item : catalogue) {
//							System.out.println("CATALOGUE");
//							System.out.println("Price: " + item.get(1));
//						}
//					}
					
					System.out.println("Catalogue contains: " + catalogue.size() + " items");
//					System.out.println("Removed price: " + r.get(1));
					
					myGui.removeBook(title, price);
				} catch (UnreadableException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
}
