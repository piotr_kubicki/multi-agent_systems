
import java.io.IOException;
import java.util.*;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class BookSellerAgent extends Agent {

	private Hashtable catalogue;
	private BookSellerGui myGui;
	private AID advertiser;
	
	protected void setup() {
		catalogue = new Hashtable();
		
		myGui = new BookSellerGui(this);
		myGui.showGui();
		
		DFAgentDescription template = new DFAgentDescription();
		ServiceDescription sd = new ServiceDescription();
		sd.setType("book-selling");
		template.addServices(sd);
		
		try {
			DFAgentDescription[] result = DFService.search(this, template);
			advertiser = result[0].getName();
		} catch (FIPAException fe) {
			fe.printStackTrace();
		}
		
		addBehaviour(new OfferRequestsServer());
		
		addBehaviour(new PurchaseOrdersServer());
		
		System.out.println("Book seller " + getAID().getName() + " is here.");
	}
	
	protected void takeDown() {
		myGui.dispose();
		System.out.println("Seller-agent " + getAID().getName() + " terminating.");
	}
	
	public void updateCatalogue(final String title, final int price) {
		addBehaviour(new OneShotBehaviour() {
			public void action() {
				
				catalogue.put(title, price);
				
				// Register book to advertiser
				ACLMessage registerBook = new ACLMessage(ACLMessage.INFORM);
				registerBook.addReceiver(advertiser);
				ArrayList book = new ArrayList();
				book.addAll(Arrays.asList(title, price, getAID()));
				try {
					registerBook.setContentObject(book);
				} catch (IOException e) {
					e.printStackTrace();
				}
				registerBook.setConversationId("book-trade");
				myAgent.send(registerBook);
			}
		});
	}
	
	private class OfferRequestsServer extends CyclicBehaviour {
		
		public void action() {
			MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.CFP);
			ACLMessage msg = myAgent.receive(mt);
			
			if (msg != null) {
				String title = msg.getContent();
				ACLMessage reply = msg.createReply();
				
				Integer price = (Integer) catalogue.get(title);

					
				if (price != null) {
					reply.setPerformative(ACLMessage.PROPOSE);
					reply.setContent(String.valueOf(price.intValue()));
				} else {
					reply.setPerformative(ACLMessage.REFUSE);
					reply.setContent("non-available");
				}
				
				myAgent.send(reply);
			} else {
				block();
			}
		}
	}
	
	private class PurchaseOrdersServer extends CyclicBehaviour {

		public void action() {
			 MessageTemplate mt =
					 MessageTemplate.MatchPerformative(ACLMessage.ACCEPT_PROPOSAL);
			 ACLMessage msg = myAgent.receive(mt);
			 
			 if (msg != null) {
				 // ACCEPT_PROPOSAL Message received. Process it
				 String title = msg.getContent();
				 ACLMessage reply = msg.createReply();
				 System.out.println("I'm " + getAID().getName() + " and I've got " + catalogue.size() + " books");
				 Integer price = (Integer) catalogue.remove(title);
					 
				if (price != null) {
					reply.setPerformative(ACLMessage.INFORM);
					System.out.println(title+" sold to agent "+msg.getSender().getName());
					
					ACLMessage advMsg = new ACLMessage(ACLMessage.INFORM);
					advMsg.addReceiver(advertiser);
					try {
						ArrayList book = new ArrayList();
						book.addAll(Arrays.asList(title, price));
						advMsg.setContentObject(book);
						advMsg.setConversationId("book-unreg");
						myAgent.send(advMsg);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
				else {
					// The requested book has been sold to another buyer in the meanwhile .
					reply.setPerformative(ACLMessage.FAILURE);
					reply.setContent("not-available");
				}
				
				myAgent.send(reply);
			 }
			 else {
				 block();
			 }
		}
	}
}

