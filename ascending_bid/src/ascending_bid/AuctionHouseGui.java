package ascending_bid;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

public class AuctionHouseGui extends JFrame {
	private AuctionHouseAgent myAgent;
	private DefaultTableModel auctioneers;
	
	public AuctionHouseGui(AuctionHouseAgent a) {
		super(a.getLocalName());
		
		myAgent = a;
		
		JPanel p = new JPanel();
		p.setLayout(new GridLayout(1, 1));
		
		getContentPane().add(p, BorderLayout.CENTER);
		
		// Make the agent terminate when the user closes 
		// the GUI using the button on the upper right corner	
		addWindowListener(new	WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				myAgent.doDelete();
			}
		} );
		
		auctioneers = new DefaultTableModel();
		auctioneers.setColumnCount(2);
		String[] columnHeaderLabel = { "Auctioneer" };
		auctioneers.setColumnIdentifiers(columnHeaderLabel);
		
		final JTable auctioneersList = new JTable(auctioneers);
		
		JScrollPane auctioneersCatalogue = new JScrollPane(auctioneersList);
		
		add(auctioneersCatalogue);
		
		setResizable(false);
	}
	
	public void addAuctioneer(String name) {
		String [] auctioneer = {name};
		auctioneers.addRow(auctioneer);
	}
	
	public void removeAuctioneer(String auctioneer) {
		ArrayList items = getRowsByValues(auctioneers, auctioneer);
		auctioneers.removeRow((int)items.get(0));
	}
	
	private ArrayList getRowsByValues(TableModel model, Object name) {
		ArrayList indexes = new ArrayList();
		
	    for (int i = model.getRowCount() - 1; i >= 0; --i) {
	        if (model.getValueAt(i, 0).equals(name)) {
	               
	        	indexes.add(i);
	        }
	    }
	    
	    return indexes;
	 }
	
	public void showGui() {
		pack();
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int centerX = (int)screenSize.getWidth() / 2;
		int centerY = (int)screenSize.getHeight() / 2;
		setLocation(centerX - getWidth() / 2, centerY - getHeight() / 2);
		super.setVisible(true);
	}	
}
