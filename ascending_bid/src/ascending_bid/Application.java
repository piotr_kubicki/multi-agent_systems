package ascending_bid;

import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;

public class Application {
	
	public static void main(String[] args){
		 //Setup the JADE environment
		 Profile myProfile = new ProfileImpl();
		 Runtime myRuntime = Runtime.instance();
		 ContainerController myContainer = myRuntime.createMainContainer(myProfile);
		 try{
		 //Start the agent controller, which is itself an agent (rma)
		 AgentController rma = myContainer.createNewAgent("rma", "jade.tools.rma.rma",
		null);
		 rma.start();
		 AgentController ah = myContainer.createNewAgent("auctionhouse",
				 AuctionHouseAgent.class.getCanonicalName(), null);
		 ah.start();

		 Object[] file1 = {"/home/pz/asc_items1.csv"};
		 AgentController ac1 = myContainer.createNewAgent("Steve",
				 AuctioneerAgent.class.getCanonicalName(), file1);
		 ac1.start();
		 
		 Object[] file2 = {"/home/pz/asc_items2.csv"};
		 AgentController ac2 = myContainer.createNewAgent("John",
				 AuctioneerAgent.class.getCanonicalName(), (Object[]) file2);
		 ac2.start();
	
		 AgentController b1 = myContainer.createNewAgent("Jeff",
				 BidderAgent.class.getCanonicalName(), null);
		 b1.start();
		 
		 AgentController b2 = myContainer.createNewAgent("Beff",
				 BidderAgent.class.getCanonicalName(), null);
		 
		 b2.start();
		}catch(Exception e){
		 System.out.println("Exception starting agent: " + e.toString());
		 }
		}
}