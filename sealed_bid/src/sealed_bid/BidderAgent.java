package sealed_bid;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

public class BidderAgent extends Agent {
	private Hashtable items; 
	private AID auctioneer;
	private AID auctionHouse;
	private String currentItem;
	private BidderGui myGui;
	
	protected void setup() {
		DFAgentDescription template = new DFAgentDescription();
		ServiceDescription sd = new ServiceDescription();
		sd.setType("auction-house");
		template.addServices(sd);
		
		try {
			DFAgentDescription[] result = DFService.search(this, template);
			
			if (result.length > 0) {
				auctionHouse = result[0].getName();
			}
		} catch (FIPAException fe) {
			fe.printStackTrace();
		}
		
		myGui = new BidderGui(this);
		
		items = new Hashtable();
		
//		loadItems((Object[][])getArguments());
		
		addBehaviour(new OfferServer());
		addBehaviour(new UnregAuctioneer());
		
		addBehaviour(new TickerBehaviour(this, 1000) {
			
			protected void onTick() {
				if (auctioneer == null && items.size() > 0) {
					addBehaviour(new FindAuctioneer());
				}
			}			
		});
		
		myGui.showGui();
	}
	
	private void loadItems(Object [][] items) {
		
		for (int i = 0; i < items.length; i++) {
			for (int j = 0; j < items[i].length; j++) {
				this.items.put((String)items[i][0], items[i][1]);
			}
		}
	}
	
	private class UnregAuctioneer extends CyclicBehaviour {
		
		public void action() {
			MessageTemplate mt = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),
					MessageTemplate.MatchConversationId("auction-end"));
			
			ACLMessage msg = myAgent.receive(mt);
		
			if (msg != null) {
				System.out.println("Get unreg request");
				auctioneer = null;
			} else {
				block();
			}
		}
	}
	
	private class FindAuctioneer extends Behaviour {
		private int step = 1;
		private MessageTemplate mt;
		
		public void action() {
			switch (step) {
			case 1:
				System.out.println("Bidder looking for auction");
				ACLMessage cfp = new ACLMessage(ACLMessage.CFP);
				cfp.addReceiver(auctionHouse);
				cfp.setConversationId("find-auctioneer");
				cfp.setReplyWith("cfp" + System.currentTimeMillis());
				myAgent.send(cfp);
				
				mt = MessageTemplate.and(MessageTemplate.MatchConversationId("find-auctioneer"),
						MessageTemplate.MatchInReplyTo(cfp.getReplyWith()));
				
				step = 2;
				break;
			case 2:
				ACLMessage reply = myAgent.receive(mt);
				
				if (reply != null) {
					if (reply.getPerformative() == ACLMessage.PROPOSE) {
						try {
							auctioneer = (AID) reply.getContentObject();
							myAgent.addBehaviour(new Register());							
						} catch (UnreadableException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else if (reply.getPerformative() == ACLMessage.FAILURE) {
						System.out.println("Auction House is empty");
					}
					step = 3;
				} else {
					block();
				}
				
				break;
			}
		}
		
		public boolean done() {
			return (step == 3);
		}
	}
	
	public void addItem(String item, int budget) {
		items.put(item, budget);
	}
	
	private class Register extends Behaviour {
		private int step = 1;
		private MessageTemplate mt;
		
		public void action() {
			switch (step) {
			case 1:
				ACLMessage reg = new ACLMessage(ACLMessage.PROPOSE);
				
				try {
					reg.addReceiver(auctioneer);
					reg.setConversationId("bider-reg");
					reg.setReplyWith("reg" + System.currentTimeMillis());
					ArrayList bider = new ArrayList();
					bider.addAll(Arrays.asList((String)myAgent.getLocalName(), (AID)myAgent.getAID()));
					reg.setContentObject(bider);
					myAgent.send(reg);
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				mt = MessageTemplate.and(MessageTemplate.MatchConversationId("bider-reg"),
						MessageTemplate.MatchReplyWith(reg.getReplyWith()));
				
				step = 2;
				break;
			case 2:
				ACLMessage response = myAgent.receive(mt);
				
				if (response != null) {
					if (response.getPerformative() == ACLMessage.CONFIRM) {
						System.out.println(myAgent.getLocalName() + " registered!");
					}
					
					step = 3;
				} else {
					block();
				}
			}
			
		}
		
		public boolean done() {
			return (step == 3);
		}
	}
	
	private class OfferServer extends CyclicBehaviour {
		
		public void action() {
			MessageTemplate mt = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),
					MessageTemplate.MatchConversationId("auction"));
			
			ACLMessage msg = myAgent.receive(mt);
			
			if (msg != null) {
				currentItem = msg.getContent();
				
				if (items.containsKey(currentItem)) {
					myAgent.addBehaviour(new Bid());
				} else {
					ACLMessage refuse = msg.createReply();
					refuse.setPerformative(ACLMessage.REFUSE);
					ArrayList offer = new ArrayList();
					offer.addAll(Arrays.asList(myAgent.getLocalName(), 0));
					
					try {
						refuse.setContentObject(offer);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					myAgent.send(refuse);
				}
			} else {
				block();
			}
		}
	}
	
	private class Bid extends Behaviour {
		private int step = 1;
		private MessageTemplate mt;
		public void action() {
			
			switch (step) {
			case 1:
				ACLMessage bid = new ACLMessage(ACLMessage.PROPOSE);
				bid.setConversationId("auction");
				bid.addReceiver(auctioneer);
				ArrayList offer = new ArrayList();
				offer.addAll(Arrays.asList(myAgent.getLocalName(), items.get(currentItem)));
				
				try {
					bid.setContentObject(offer);
					myAgent.send(bid);
					step = 2;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				break;
			case 2:
				mt = MessageTemplate.MatchConversationId("auction");
				
				ACLMessage result = myAgent.receive(mt);
				
				if (result != null) {
					if (result.getPerformative() == ACLMessage.CONFIRM) {
						System.out.println("I won " + myAgent.getLocalName());
						String item = result.getContent();
						myGui.removeItem(item);
					}
					step = 3;
				} else {
					block();
				}
				
				break;
			}
		}
		
		public boolean done() {
			return (step == 3);
		}
			
	}

}

